/**
 * @file  header.h
 * @brief Description of data types using in the test task solution.
 *
 * @details
 *
 * @author  Grigoriy Romanov (gr), romanov.grgn@gmail.ru
 *
 * @internal
 * @date  2020.05.29
 *     Company  Saint Petersburg, Russia
 * @copyright  Copyright (c) 2020, Saint Petersburg, Russia
 *
 * =====================================================================================
 */

#ifndef _HEADER_TT_
#define _HEADER_TT_

#include <iostream>
#include <cmath>
#include <cstring>
#include <regex>
#include <map>
#include <vector>

/**
 * @brief   Class intented to create a specified number of prime numbers.
 * @details From class's constructor prime numbers sieve is creating using Atkin's
 *          method. There is a class method to fill specified array with specified
 *          size.
 * @note    For primes storage unsigned integer type is using.
 */
class Primes {
    public:
        Primes (unsigned int _limit);
        void fillArray (unsigned int N, unsigned int *& array);
    private:
        void calculate_sieve ( void );

        bool *       sieve;
        unsigned int limit;
        unsigned int foundPrimes;
};

/**
 * @brief Function corresponding to a second task. Counts defferent-length words
 *        and writes a std::map<int,int> with a number of each word with a
 *        specified length.
 */
void word_counter(std::string text, std::map<int,int> & map);

/**
 * @brief Struct as a node for implemented list for third task.
 */
template<typename P>
struct List_en_t {
    struct List_en_t * next;
    P payload;
};

/**
 * @brief Contains unidirectional list that being allocated in constructor with
 *        specified length.
 */
template<typename T>
class UnidirectList {
    public:
        /**
         * @brief Creates unidirectional list of List_en_t<T> nodes with
         *        specified length. Head of the list is member of the class.
         */
        UnidirectList (int _length) :
                      head(nullptr) {
            head = new List_en_t<T>;
            head->payload = (T)(1);
            List_en_t<T> * end = head;
            for (int i = 1; i < _length; i++) {
                end->next = new List_en_t<T>;
                end = end->next;
                end->payload = (T)(i+1);
                end->next = nullptr;
            }
        };

        ~UnidirectList () {
            removeList();
        };

        /**
         * @brief Method remove each M entry from alredy created list;
         *        M must be >= 2 and less than the list length;
         */
        void removeEach (int M) {
            if ( M <= 1 ) {
                std::cout << "\033[1;31mToo small argument: "
                          << M << ". At:" << __PRETTY_FUNCTION__
                          << "Aborted.\033[0;m" << std::endl;
                return;
            }
            if ( M > getLength() ) {
                std::cout << "\033[1;33mThere are no entries multuples passed value: "
                          << M << ". At:" << __PRETTY_FUNCTION__
                          << "Aborted.\033[0;m" << std::endl;
                return;
            }

            List_en_t<T> * tmp;
            List_en_t<T> * cur = head;
            int cnt = 1;
            while (cur->next) {
                if ( ((cnt+1) % M == 0) ) {
                    tmp = cur->next;
                    cur->next = tmp->next;
                    delete tmp;
                    // count just removed element
                    cnt++;
                }
                if (cur->next) {
                    cur = cur->next;
                    cnt++;
                }
            }
        };

        /**
         * @brief Self documented.
         */
        int  getLength (void) {
            int length = 0;
            List_en_t<T> * end = head;
            while (end->next != nullptr) { length++; end = end->next; };
            return ++length;
        };

        /**
         * @brief Self documented.
         */
        void removeList (void) {
            List_en_t<T> * cur;
            List_en_t<T> * tmp;
            while (head->next != nullptr) {
                cur = head;
                while(cur->next->next != nullptr) {
                    cur = cur->next;
                }
                tmp = cur->next;
                cur->next = nullptr;
                delete tmp;
            }
            delete head;
        }

        /**
         * @brief Self documented.
         */
        List_en_t<T> * getHead () {return head;};
    private:
        List_en_t<T> * head;
};

/**
 * @brief Function related to a fourth task. Returns a max value of an unsigned
 *        integer that could be represented using one's bits of argument's value.
 */
unsigned int max_uint(unsigned int N);

/**
 * @brief Using for parameterize data type of tree's node value.
 */
typedef int T;

/**
 * @brief Node of a binary tree.
 */
struct node{
	T    value;
	node *left;
	node *right;
};

/**
 * @brief Implements binary search tree structure. Use @ref node as
 *        nodes. Has methods that allows find out max depth of the tree
 *        and print out exact paths with specified depth.
 */
class BTree{
public:
	BTree();
	~BTree();

	void insert(T key);
	node *search(T key);
	void destroyTree();
	void inorderPrint();
	void postorderPrint();
	void preorderPrint();
    int  findMaxDepth();
    void printPaths(int depth);

private:
	void destroyTree(node *leaf);
	void insert(T key, node *leaf);
	node *search(T key, node *leaf);
	void inorderPrint(node *leaf);
	void postorderPrint(node *leaf);
	void preorderPrint(node *leaf);
    int  findMaxDepth(node *leaf);
    void printPaths(node *leaf, int depth);
    void printOutPath();

    std::vector<T> container;

	node *root;
};

#endif //_HEADER_TT_
