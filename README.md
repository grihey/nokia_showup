# nokia_showup

Test task for Nokia Solutions aims to show up a basic C/C++ skills.

<b>Assumptions</b> is laying in the solution:

##### Fourth task
Minimum value of unsigned integer type everytime is zero, so it was skipped. Related
funciton `max_uint( usigned )` returns only maximum value that could be obtained
using one's bits of unsigned int value passing as argument.

##### Five task
In the task description struct of a node has no payload field, so I've implemented
it. Also the description has no mention about exact type of a binary tree, I've
chosen binary search tree (BST) to work with.

### Building

##### Prerequsites

Linux environment - the solution has not been tested on Windows OS.<br>
cmake - (worked with <b>3.16.3</b> version) <br>
g++ - (worked with <b>9.3.0</b>) <br>

Environment variable <b>CXX</b> should be set to reflect a compiler path or a compiler
name if a path or your compiler already in the <b>PATH</b>.

##### Possible way to build

from repository src folder:

`export CXX=$(which g++)` <br>
`mkdir build; cd build` <br>
`cmake ../` <br>
`make`

`cmake` command will generate CMakeLists.txt which will download a googletests sources, so an internet connection is needed.

<b>Gtest version</b> is using - <b>1.10.0</b>

### Run
After build sucessively type from build folder:
`./TestTask`

### Documentation

Project has a poor documentation based on doxygen.<br>
To build documentation:

`cd doc` <br>
`doxygen Doxyfile`

Documentation will be at doc/generated folder.
