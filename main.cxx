/**
 * @file  main.cxx
 * @brief Entry point of test task solution. Contains whole bunch of created tests.
 *
 * @details
 *
 * @author  Grigoriy Romanov (gr), romanov.grgn@gmail.ru
 *
 * @internal
 * @date  2020.05.29
 *     Company  Saint Petersburg, Russia
 * @copyright  Copyright (c) 2020, Saint Petersburg, Russia
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <iostream>
#include <cmath>

#include "gtest/gtest.h"
#include "header.h"

/**
 * @brief Test create class with limit equals 100 and fill array with 10 primes.
 *        Then try to check if a remainder from a division on number is
 *        differentiating from one and itself is zero.
 */
TEST(TestingTestFirstTask, Case1) {
    Primes * primes_inst = new Primes(100UL);

    unsigned int * targetArray;
    unsigned int N = 10;
    primes_inst->fillArray(N, targetArray);

    EXPECT_EQ(targetArray[0],2UL);

    for (int i = 1; i < N; i++) {
        for (int j = 2; j < targetArray[i]; j++) {
            int rem = targetArray[i] % j;
            EXPECT_NE(rem,0);
        }
    }
}

/**
 * @brief Test create class with limit equals 1000 and fill array with 168 primes.
 *        Then try to check if a remainder from a division on number is
 *        differentiating from one and itself is zero.
 */
TEST(TestingTestFirstTask, Case2) {
    Primes * primes_inst = new Primes(1000UL);

    unsigned int * targetArray;
    unsigned int N = 169;
    primes_inst->fillArray(N, targetArray);

    EXPECT_EQ(targetArray[0],2UL);

    for (int i = 1; i < N; i++) {
        for (int j = 2; j < targetArray[i]/2; j++) {
            EXPECT_NE(targetArray[i],0);
            int rem = targetArray[i] % j;
            EXPECT_NE(rem,0);
        }
    }
}

/**
 * @brief Test creates text consist of four 4-character words with commas and dot
 *        then checks if only one entry being created with key and value equals 4.
 */
TEST(TestingTestSecondTask, Case1) {
    std::map<int,int> map_dict;
    std::string str{"Four, four, four, four."};
    word_counter(str, map_dict);
    EXPECT_EQ(map_dict.size(),1);
    EXPECT_EQ(map_dict.at(4),4);
}

/**
 * @brief Test creates a text with 5 different lengthed words. Then checks if map
 *        has 5 entries with 1 as value.
 */
TEST(TestingTestSecondTask, Case2) {
    std::map<int,int> map_dict;
    std::string str{"O, ne, two, thre, efour."};
    word_counter(str, map_dict);
    EXPECT_EQ(map_dict.size(),5);
    EXPECT_EQ(map_dict.at(1),1);
    EXPECT_EQ(map_dict.at(2),1);
    EXPECT_EQ(map_dict.at(3),1);
    EXPECT_EQ(map_dict.at(4),1);
    EXPECT_EQ(map_dict.at(5),1);
}

/**
 * @brief Creates a list with 100 nodes and check if a length of the list
 *        equals 100.
 */
TEST(TestingTestThirdTask, Case1) {
    UnidirectList<int> list_i(100);
    int length = list_i.getLength();
    EXPECT_EQ(length, 100);
}

/**
 * @brief Creates a list with 100 nodes, then remove each fifth and check that
 *        length has been changed to 80.
 */
TEST(TestingTestThirdTask, Case2) {
    UnidirectList<int> list_i(100);
    list_i.removeEach(5);
    int length = list_i.getLength();
    EXPECT_EQ(length, 80);
}

/**
 * @brief Creates a list with 100 nodes, then remove each fifth and check that
 *        payload of entries has no values multiples five.
 * @note  Each created entry of the list has an integer-typed payload with
 *        increment order is starting from one.
 */
TEST(TestingTestThirdTask, Case3) {
    UnidirectList<int> list_i(100);
    list_i.removeEach(5);
    List_en_t<int> * entry = list_i.getHead();
    while(entry->next != nullptr) {
        EXPECT_NE(entry->payload % 5, 0);
        entry = entry->next;
    }
}

/**
 * @brief call a function max_uint() with 0xffffffff enter value. excpected maximum
 *        obtained value as 4294967295.
 */
TEST(TestingTestFourthTask, Case1) {
    unsigned int N = 0xFFFFFFFF;
    EXPECT_EQ(max_uint(N), 4294967295);
}

/**
 * @brief call a function max_uint() with 0xAAA00555 enter value. excpected maximum
 *        obtained value as 4095.
 */
TEST(TestingTestFourthTask, Case2) {
    unsigned int N = 0xAAA00555;
    EXPECT_EQ(max_uint(N), 4095);
}

/**
 * @brief Creates binary tree in exact order test expect that structure will be:
 *                              10
 *                             /  \
 *                            /    \
 *                           6     14
 *                          / \   /  \
 *                         5   8 11   18
 *        so maximum depth should be: 3.
 */
TEST(TestingTestFiveTask, Case1) {
	BTree *tree = new BTree();

	tree->insert(10);
	tree->insert(6);
	tree->insert(14);
	tree->insert(5);
	tree->insert(8);
	tree->insert(11);
	tree->insert(18);

    int depth = tree->findMaxDepth();
    EXPECT_EQ(depth,3);
    tree->printPaths(depth);

	delete tree;
}

/**
 * @brief Test creates chain by inserting numbers in inclementing order up to 100.
 *        Expected structure of the tree is a chain. So max depth will be: 100.
 */
TEST(TestingTestFiveTask, Case2) {
	BTree *tree = new BTree();
    for (int i = 0; i < 100; i++) {
        tree->insert(i);
    }
    int depth = tree->findMaxDepth();
    EXPECT_EQ(depth, 100);

    tree->printPaths(depth);
    delete tree;
}

int main ( int argc, char ** argv ) {
    testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}
