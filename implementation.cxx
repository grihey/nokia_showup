/**
 * @file  implementation.cxx
 * @brief File with implementation of using functions and classe's methods.
 *
 * @details
 *
 * @author  Grigoriy Romanov (gr), romanov.grgn@gmail.ru
 *
 * @internal
 * @date  2020.05.29
 *     Company  Saint Petersburg, Russia
 * @copyright  Copyright (c) 2020, Saint Petersburg, Russia
 *
 * =====================================================================================
 */


#include "header.h"

Primes::Primes (unsigned int _limit) :
    limit(_limit) {
    sieve = new bool[limit];

    // init a sieve
    std::fill(sieve, sieve + limit, false);
    sieve[2] = true;
    sieve[3] = true;

    calculate_sieve();
}

/**
 * @brief Fill passed array with specified number of primes.
 * @note  If requested number of primes more that has already been found, array
 *        passing will be filled with all sieved primes.
 */
void Primes::fillArray(unsigned int N, unsigned int *& arr) {
    unsigned int i = 0, curN = 0;
    if  (N > foundPrimes) {
        std::cout << "\033[1;31mClass has no enough primes to fill array"
                  << " with requested quantity: " << N << "\033[0;m"
                  << std::endl;
        std::cout << "\033[1;33mSize of target array will be changed to number "
                  << "of already found primes: " << foundPrimes << "\033[0;m"
                  << std::endl;

        N = foundPrimes;
    }
    if (arr) {
        arr = new unsigned int[N];
        memset(arr,0,N);

        while (curN < N) {
            if (sieve[i]) {
                arr[curN++] = i;
            }
            i++;
        }
    }
}

/**
 * @brief Method implement Atkin sieve algorithm.
 * @details Creates only a sieve without numbers itself. Range of sieve based
 *          on @ref limit which is initializing at a constructor.
 *          Algorithm based on one from wikipedia.org.
 *          https://ru.wikipedia.org/wiki/Решето_Аткина but has tiny fixes.
 * @note Limit will be cast to a double to find self square root.
 */
void Primes::calculate_sieve(void) {
    int x2 = 0, y2 = 0;
    unsigned n = 0;
    unsigned sqrt_lim = (unsigned)(ceil(sqrt((double)limit)));

    for (int i = 1; i < sqrt_lim; i++) {
        x2 += 2 * i - 1;
        y2 = 0;
        for (int j = 1; j < sqrt_lim; j++) {
            y2 += 2 * j - 1;
            n = 4 * x2 + y2;
            if ((n <= limit) && (n % 12 == 1 || n % 12 == 5)) {
                sieve[n] = !sieve[n];
            }

            n -= x2;
            if ((n <= limit) && (n % 12 == 7)) {
                sieve[n] = !sieve[n];
            }

            n -= 2 * y2;
            if ((i > j) && (n <= limit) && (n % 12 == 11)) {
                sieve[n] = !sieve[n];
            }
        }
    }
    // Algorithm above can't reject multiples of prime's squares, so do it below.
    for (int i = 5; i <= sqrt_lim; i++) {
        if (sieve[i]) {
            n = i * i;
            for (int j = n; j <= limit; j += n) {
                sieve[j] = false;
            }
        }
    }
    // calculate number of primes have been found
    for (int i = 0; i <= limit; i++) {
        if (sieve[i]) {
           foundPrimes++;
        }
    }
}

/**
 * @brief Function classifies words in text by its length and counts number of
 *        a same length words.
 * @details Results being stored in passed as reference std::map in form
 *          [word_length:number].
 */
void word_counter(std::string text, std::map<int,int> & map) {
    std::regex  regex("\\W+");
    std::sregex_token_iterator it{text.begin(), text.end(), regex, -1};
    std::vector<std::string> words{it, {}};
    for (std::vector<std::string>::const_iterator i = words.begin(); i != words.end(); ++i) {
        int len = (*i).length();
        if ( map.find(len) == map.end() ) {
          // not found
          map.insert(std::make_pair(len,1));
        } else {
          // found
          map.at(len)++;
        }
    }
}

/**
 * @brief Function related to a fourth task. Returns a max value of an unsigned
 *        integer that could be represented using one's bits of argument's value.
 */
unsigned int max_uint(unsigned int N) {
    unsigned long long muint = 1;
    for (int i = 0; i < sizeof(N) * 8; i++) {
        if (N & 0x1) {
            muint <<= 1;
        }
        N >>= 1;
    }
    return (unsigned int)(muint - 1);
}

BTree::BTree(){
	root = nullptr;
}

BTree::~BTree(){
	destroyTree();
}

void BTree::destroyTree(node *leaf){
	if(leaf != nullptr){
		destroyTree(leaf->left);
		destroyTree(leaf->right);
		delete leaf;
	}
}

void BTree::destroyTree(){
	destroyTree(root);
}

void BTree::insert(T key, node *leaf){

	if(key < leaf->value){
		if(leaf->left != nullptr){
			insert(key, leaf->left);
		}else{
			leaf->left = new node;
			leaf->left->value = key;
			leaf->left->left = nullptr;
			leaf->left->right = nullptr;
		}
	}else if(key >= leaf->value){
		if(leaf->right != nullptr){
			insert(key, leaf->right);
		}else{
			leaf->right = new node;
			leaf->right->value = key;
			leaf->right->right = nullptr;
			leaf->right->left = nullptr;
		}
	}
}

void BTree::insert(T key){
	if(root != nullptr){
		insert(key, root);
	}else{
		root = new node;
		root->value = key;
		root->left = nullptr;
		root->right = nullptr;
	}
}

node *BTree::search(T key, node *leaf){
	if(leaf != nullptr){
		if(key == leaf->value){
			return leaf;
		}
		if(key < leaf->value){
			return search(key, leaf->left);
		}else{
			return search(key, leaf->right);
		}
	}else{
		return nullptr;
	}
}

node *BTree::search(T key){
	return search(key, root);
}

void BTree::inorderPrint(node *leaf){
	if(leaf != nullptr){
		inorderPrint(leaf->left);
        std::cout << leaf->value << ",";
		inorderPrint(leaf->right);
	}
}

void BTree::inorderPrint(){
	inorderPrint(root);
    std::cout << "\n";
}



void BTree::postorderPrint(node *leaf){
	if(leaf != nullptr){
		inorderPrint(leaf->left);
		inorderPrint(leaf->right);
        std::cout << leaf->value << ",";
	}
}

void BTree::postorderPrint(){
	postorderPrint(root);
    std::cout << "\n";
}

void BTree::preorderPrint(node *leaf){
	if(leaf != nullptr){
        std::cout << leaf->value << ",";
		inorderPrint(leaf->left);
		inorderPrint(leaf->right);
	}
}

void BTree::preorderPrint(){
	preorderPrint(root);
    std::cout << "\n";
}

int BTree::findMaxDepth(node *leaf){
    static int lvl = 0;
    static int max_depth = 0;
	if(leaf != nullptr){
        lvl++;
		findMaxDepth(leaf->left);
        if (lvl > max_depth) { max_depth = lvl; };
		findMaxDepth(leaf->right);
        --lvl;
	}
    return max_depth;
}

int BTree::findMaxDepth() {
    return findMaxDepth(root);
}

void BTree::printPaths(node *leaf, int depth) {
    if (leaf != nullptr) {
        container.push_back(leaf->value);
        if (container.size() == depth) {
            printOutPath();
        }
        printPaths(leaf->left, depth);
        printPaths(leaf->right, depth);
        container.pop_back();
    };
}

void BTree::printPaths(int depth) {
    printPaths(root, depth);
}

void BTree::printOutPath() {
    std::cout << "Max path is: ";
    for (int i = 0; i < container.size(); i++) {
        std::cout << container.at(i) << " ";
    }
    std::cout << std::endl;
}
